import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as mani
from matplotlib.widgets import Slider, Button, RadioButtons

class anim:
    def __init__(self,data,title,num_axis=0,num_vaxis=1,num_haxis=1,interval=40,max_count=100,wraparound=True,save=False):
        self.fig,self.ax= plt.subplots(num_vaxis,num_haxis)
        self.num_vaxis  = num_vaxis
        self.num_haxis  = num_haxis
        self.data       = data
        self.title      = title
        self.max_count  = max_count
        self.counter    = 0
        if num_haxis > 1 or num_vaxis > 1:
            self.pos        = self.ax[0].get_position()
        else:
            self.pos        = self.ax.get_position()
        self.running    = False
        self.reversed   = False
        self.markmin    = 0
        self.markmax    = self.max_count
        self.wraparound = wraparound
        self.save       = save

        self.timer = self.fig.canvas.new_timer(interval=interval)
        self.timer.add_callback(self.update, self.ax)
        self.FPS        = 1000./self.timer.interval

        self.axsl       = self.fig.add_axes([   self.pos.x0,
                                                self.pos.y0-0.1,
                                                self.pos.x1-self.pos.x0,
                                                self.pos.y0-0.09])
        self.sl         = Slider(   self.axsl,
                                    '',
                                    0,
                                    self.max_count-1,
                                    valinit=self.counter,
                                    valstep=1,
                                    valfmt="%d",
                                    dragging=False)
        self.sl.on_changed(self.on_change_slider)
        self.fig.canvas.mpl_connect('key_press_event', self.press)
        if num_haxis > 1 or num_vaxis > 1:
            self.ax[0].set_title(self.title[self.counter]+" FPS {}".format(np.int(self.FPS)))
        else:
            self.ax.set_title(self.title[self.counter]+" FPS {}".format(np.int(self.FPS)))

    def animate(self,lines,update):
        self.up_func = update
        self.lines = lines
        self.running    = True
        self.timer.start()
    
    def on_change_slider(self,val):
        self.running = False
        self.counter = np.int(val)
        self.up_func(   self.ax,
                        self.lines,
                        self.data,
                        self.title[self.counter]+" FPS {}".format(np.int(self.FPS)),
                        self.counter)
        if self.save:
            self.fig.savefig("anim{:05d}.png".format(val),dpi=300,bbox_inches="tight")

    def demo_update_scale(ax,li,data,title,num):
        li[0].set_data(data[0][num].T)
        li[0].autoscale()
        ax.draw_artist(li[0])
        if self.num_haxis > 1 or self.num_vaxis > 1:
            ax[0].set_title(title)
        else:
            ax.set_title(title)

    def demo_update_noscale(ax,li,data,title,num):
        li[0].set_data(data[0][num].T)
        ax.draw_artist(li[0])
        if self.num_haxis > 1 or self.num_vaxis > 1:
            ax[0].set_title(title)
        else:
            ax.set_title(title)

    def inc(self):
        self.counter = self.counter + 1
        if self.counter >= self.markmax:
            if self.wraparound:
                self.counter = self.markmin
            else:
                self.counter    = self.counter - 1
                self.reversed   = True

    def dec(self):
        self.counter = self.counter - 1
        if self.counter < self.markmin:
            if self.wraparound:
                self.counter = self.markmax-1
            else:
                self.counter    = self.counter + 1
                self.reversed   = False

    def press(self,event):
        if event.key == ' ':
            if self.running:
                self.running = False
            else:
                self.running = True
        elif event.key == '.' and not(self.running):
            self.inc()
            self.sl.set_val(self.counter) 
        elif event.key == ',' and not(self.running):
            self.dec()
            self.sl.set_val(self.counter) 
        elif event.key == 'i':
            self.inc_timer()
        elif event.key == 'd':
            self.dec_timer()
        elif event.key == 'r' and self.running:
            if self.reversed:
                self.reversed = False
            else:
                self.reversed = True
        elif event.key == 'm':
            if self.markmin == 0:
                self.markmin = self.counter
            elif self.markmax == self.max_count:
                self.markmax = self.counter
            else:
                self.markmax = self.max_count
                self.markmin = 0
        elif event.key == 'w':
            if self.wraparound:
                self.wraparound = False
            else:
                self.wraparound = True

    def set_FPS(self,FPS=25):
        if FPS <= 50 and FPS > 0:
            self.FPS    = FPS
            self.timer.interval = 1000./self.FPS 
            if self.num_haxis > 1 or self.num_vaxis > 1:
                self.ax[0].set_title(self.title[self.counter]+" FPS {}".format(np.int(self.FPS)))
            else:
                self.ax.set_title(self.title[self.counter]+" FPS {}".format(np.int(self.FPS)))

    def inc_timer(self):
        if self.FPS < 50:
            self.FPS    = self.FPS + 1
            self.timer.interval = 1000./self.FPS 
            if self.num_haxis > 1 or self.num_vaxis > 1:
                self.ax[0].set_title(self.title[self.counter]+" FPS {}".format(np.int(self.FPS)))
            else:
                self.ax.set_title(self.title[self.counter]+" FPS {}".format(np.int(self.FPS)))
            
    def dec_timer(self):
        if self.FPS > 1 :
            self.FPS    = self.FPS - 1
            self.timer.interval = 1000./self.FPS 
            if self.num_haxis > 1 or self.num_vaxis > 1:
                self.ax[0].set_title(self.title[self.counter]+"FPS {}".format(np.int(self.FPS)))
            else:
                self.ax.set_title(self.title[self.counter]+"FPS {}".format(np.int(self.FPS)))

    def update(self,axis):
        if self.running:
            if self.reversed:
                self.dec()
            else:
                self.inc()
            self.sl.set_val(self.counter)
            self.running = True
