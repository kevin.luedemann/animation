from setuptools import setup

setup(name='animation',
      version='0.1',
      description='Animation class for video like controlle',
      url='https://gitlab.gwdg.de/kevin.luedemann/animation.git',
      author='Kevin Luedemann',
      author_email='kevin.luedemann@stud.uni-goettingen.de',
      license='BSD-3',
      packages=['animation'],
      install_requires=[
        'numpy',
        'numpydoc',
        'matplotlib',
        'scipy',
        'sphinx'
      ],
      zip_safe=False)

