import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.animation as mani
from matplotlib.widgets import Slider, Button, RadioButtons
from animation import anim

def update(ax,line,data,title,num):
    for i in range(len(line)):
        line[i].set_ydata(data[i][num])

def main():
    N       = 256
    dx      = 2.*np.pi/np.float(N)
    x       = np.arange(0.,2.*np.pi+dx,dx)
    field1  = np.sin(x)
    field2  = np.cos(x)
    field3  = np.sin(x)*np.cos(x)
    amp     = np.arange(0.,1.+1./100.,1./100.)
    title   = []
    data1   = []
    data2   = []
    data3   = []
    for am in amp:
        data1.append(field1*am)
        data2.append(field2*am)
        data3.append(field3*am)
        title.append("")
        
    data1    = np.array(data1)
    data2    = np.array(data2)
    data3    = np.array(data3)

    ani = anim.anim([data1,data2,data3],
                    title,
                    num_axis=1,
                    interval=40,
                    max_count=data1.shape[0])

    line1, = ani.ax.plot(   x,
                            data1[0],
                            "r",
                            label="sin(x)")
    line2, = ani.ax.plot(   x,
                            data2[0],
                            "b",
                            label="cos(x)")
    line3, = ani.ax.plot(   x,
                            data3[0],
                            "k",
                            label="sin(x)cos(x)")
    ani.ax.set_ylim(-1.,1.)
    ani.ax.grid(True)
    ani.ax.legend(loc="best")
    ani.animate([line1,line2,line3],update)

    plt.show()

if __name__ == "__main__":
    main()
